fn main() {
    let a_state = StateMachine::default();
    println!("state: {}", a_state.state.name);
    let b_state = StateMachine::<B>::from(a_state);
    println!("state: {}", b_state.state.name);
    let c_state = StateMachine::<C>::from(b_state);
    println!("state: {}", c_state.state.name);
    let a_state = StateMachine::<A>::from(c_state);
    println!("state: {}", a_state.state.name);
}

struct StateMachine<S> {
    state: S,
}

struct A {
    name: String,
}

struct B {
    name: String,
}

struct C {
    name: String,
}

impl Default for StateMachine<A> {
    fn default() -> Self {
        Self {
            state: A {
                name: "A".to_string(),
            },
        }
    }
}

impl From<StateMachine<A>> for StateMachine<B> {
    fn from(value: StateMachine<A>) -> Self {
        Self {
            state: B {
                name: "B".to_string(),
            },
        }
    }
}

impl From<StateMachine<B>> for StateMachine<C> {
    fn from(value: StateMachine<B>) -> Self {
        Self {
            state: C {
                name: "C".to_string(),
            },
        }
    }
}

impl From<StateMachine<C>> for StateMachine<A> {
    fn from(value: StateMachine<C>) -> Self {
        Self {
            state: A {
                name: "A".to_string(),
            },
        }
    }
}
